var myApp = angular.module('myApp', ['ngRoute','QuizModule'])
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'Quiz/quiz.tpl.html',
                    controllerAs: 'ctrl',
                    controller: 'QuizController'
                });
        }
    ]);





