(function(){
    var quizModule = angular.module('QuizModule');

    quizModule.factory("QuizFactory",['ItunesService','$q', function(ItunesService,$q){


        var QUIZ_ROUNDS = 5;

        var ARTIST_LIST = ["Drake", "Metallica", "Infected Mushroom", "Led Zeppelin", "Pearl Jam", "Radiohead", "Frank Sinatra",
            "Sia", "Soundgarden", "U2", "Green Day", "Beastie Boys", "Foo Fighters", "Moby", "Rihanna",
            "2Pac", "Nina Simone", "Pharrell Williams", "Led Zeppelin","Marvin Gaye"];


        var totalScore, currentRound;


        //get random integer inclusive method
        function getRandomInt(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function getTotalScore(){
            return totalScore;
        }

        function updateTotalScore(correct,attempts){
            if ( correct){
                switch(attempts) {
                    case 1:
                        totalScore += 5;
                        break;
                    case 2:
                        totalScore += 3
                        break;

                    case 3:
                        totalScore +=1;
                        break;
                }

            }
        }

        function updateRoundNumber(){
            currentRound++;
            return currentRound;
        }

        function chooseRandomArtist(){
            var artistName = ARTIST_LIST.splice(getRandomInt(0,ARTIST_LIST.length - 1),1);
            return artistName[0];
        }

        function initQuiz(){
            totalScore = 0;
            currentRound = 0;
        }

        function checkQuizProgressStatus(){
            return currentRound <= QUIZ_ROUNDS;
        }

        return{
            getTotalScore : getTotalScore,                  //  return quiz total score
            updateTotalScore : updateTotalScore,            //  update quiz total score
            nextRound : updateRoundNumber,                  //  increment round indicator
            getArtistFromList : chooseRandomArtist,         //  random choose artist for question
            startQuiz : initQuiz,                           //  init quiz settings
            isQuizInProgress : checkQuizProgressStatus      //  return false if number of round exceeded
        }

    }]);

})();