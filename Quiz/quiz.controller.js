(function(){

    var quizModule = angular.module('QuizModule');

    quizModule.controller('QuizController',['QuizFactory','ItunesService', quizController]);


    function quizController(QuizFactory,ItunesService){

        var NO_OF_ALBUMS = 3;

        var vm  = this;

        //show final score flag
        vm.showFinalScore = false;

        /*
         get current question value
         */
        vm.currentQuestionPoint = function(){
            var points = 0 ;

            if(vm.questionDetails){

                switch(vm.questionDetails.answerAttempts) {
                    case 0:
                        points = 5;
                        break;
                    case 1:
                        points = 3
                        break;

                    case 2:
                        points = 1;
                        break;
                    default:
                        points = 5;
                        break;

                }
            }

            return points;

        };

        /*
         check if answer is valid
         */
        vm.checkAnswer = function(){

            if(vm.userAnswer && vm.userAnswer.toLowerCase() === vm.questionDetails.answer.toLowerCase()){
                QuizFactory.updateTotalScore(true,vm.questionDetails.answerAttempts +1);
                getRoundData();
                return;
            }

            vm.questionDetails.answerAttempts++

            if (vm.questionDetails.answerAttempts === 3){
                QuizFactory.updateTotalScore(false,vm.questionDetails.answerAttempts);
                getRoundData();
            }


        }


        /*
            QuizQuestion Representational Object
         */
        function QuizQuestion(resultSet){
            this.question = "Who\'s the artist? (enter full name)";
            this.answer = resultSet ? resultSet[0].artistName : "";
            this.options  = [];

            for (var albumIndex = 0; albumIndex < resultSet.length; albumIndex++) {
                this.options.push({
                    name: resultSet[albumIndex].collectionName,
                    hint: resultSet[albumIndex].artworkUrl100
                });
            }
            this.answerAttempts = 0;
        }


        /*
            move to next phase in quiz
            fetch
         */
        function getRoundData(){

            vm.quizRound = QuizFactory.nextRound();

            if(  QuizFactory.isQuizInProgress()){

                vm.userAnswer = "";
                var artist = QuizFactory.getArtistFromList();
                ItunesService.getArtistDetails(artist,NO_OF_ALBUMS).then(function(data){
                    vm.questionDetails = new QuizQuestion(data.data.results);
                    vm.showOptionPane = true;
                });
            }
            else{
                --vm.quizRound; //for display only
                //quiz over
                vm.totalScore = "Game Over!, Your Total Score: " + QuizFactory.getTotalScore();
                vm.showFinalScore = true;
            }

        }

        function startQuiz(){
            QuizFactory.startQuiz();
            getRoundData();
        }




        startQuiz();
    };

})();
