
(function() {
    var quizModule = angular.module('QuizModule',[]);

    quizModule.service('ItunesService', ['$http', ItunesService]);


    function ItunesService($http){


        var ITUNES_API  = "https://itunes.apple.com/";

        var ITUNES_CONFIG = {
            params : {
                "callback" : "JSON_CALLBACK"
            }
        };



        this.getArtistDetails = function(artistName,noOfAlbums){

            var apiArtistName = artistName.replace(" ","+");

            var artistDetailsConfig = {
                params : {
                    "callback" : "JSON_CALLBACK",
                    "term" : apiArtistName,
                    "country" : "IL",
                    "media" : "music",
                    "entity" : "album",
                    "limit" : noOfAlbums || 3
                }
            };


            var promise = $http.jsonp(ITUNES_API + "search", artistDetailsConfig);

            return promise;

        }


    }

})();